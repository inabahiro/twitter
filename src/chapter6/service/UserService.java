package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.User;
import chapter6.dao.UserDao;
import chapter6.utils.CipherUtil;

public class UserService {


    public void insert(User user) {

        Connection connection = null;
        try {

            // パスワード暗号化（CipherUtilのencrypt()メソッド）
            String encPassword = CipherUtil.encrypt(user.getPassword());
            user.setPassword(encPassword);

            //ユーザー情報をDBに登録 (UserDaoのinsert()メソッドおよびDBUtil.javaの各種メソッド）
            connection = getConnection();
            new UserDao().insert(connection, user);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    //ログイン
    public User select(String accountOrEmail, String password) {

        Connection connection = null;
        try {
            // パスワード暗号化
            String encPassword = CipherUtil.encrypt(password);

            connection = getConnection();

            //UserDao.select()メソッドで入力されたログイン情報の確認
            User user = new UserDao().select(connection, accountOrEmail, encPassword);
            commit(connection);

            return user;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
     }


    public User select(int userId) {

        Connection connection = null;
        try {
            connection = getConnection();
            User user = new UserDao().select(connection, userId);
            commit(connection);

            return user;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    //ユーザ情報の更新
    public void update(User user) {

        Connection connection = null;
        try {

        	//課題①追加
        	if(!StringUtils.isEmpty(user.getPassword())){
        		// パスワード暗号化
        		String encPassword = CipherUtil.encrypt(user.getPassword());
        		//暗号化パスワードを登録
        		user.setPassword(encPassword);
        	}

            //コネクションの取得
            connection = getConnection();

            //UserDaoのupdateメソッド実行
            new UserDao().update(connection, user);

            commit(connection);

        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    //アカウントの重複確認
    public User select(String account) {

        Connection connection = null;

        try {
        	//DB接続
            connection = getConnection();

            //UserDaoのselectメソッド
            User user = new UserDao().select(connection, account);

            commit(connection);
            return user;

        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

}
