package chapter6.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import chapter6.beans.User;
import chapter6.beans.UserComment;
import chapter6.beans.UserMessage;
import chapter6.service.CommentService;
import chapter6.service.MessageService;



@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

    	//つぶやきの表示

        User user = (User) request.getSession().getAttribute("loginUser");

        //ログイン中のみtop.jspでつぶやき、返信フォームを表示
        boolean isShowMessageForm = false;
        if (user != null) {
            isShowMessageForm = true;
        }
        request.setAttribute("isShowMessageForm", isShowMessageForm);

        //絞り込み
        String start = request.getParameter("start");	//top.jspからyyyy-MM-ddのフォーマットで取得できる
        String end = request.getParameter("end");
        //値の保持
        request.setAttribute("start", start);
        request.setAttribute("end", end);

        //課題②
        String userId = request.getParameter("user_id");

        List<UserMessage> messages = new MessageService().select(userId, start, end);
        request.setAttribute("messages", messages);

        //コメントの表示
        List<UserComment> comments = new CommentService().select();
        request.setAttribute("comments", comments);

        //top.jspへフォワード
        request.getRequestDispatcher("/top.jsp").forward(request, response);
    }
}