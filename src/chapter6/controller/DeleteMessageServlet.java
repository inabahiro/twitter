package chapter6.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import chapter6.service.MessageService;

//つぶやきの削除
@WebServlet("/deleteMessage")
public class DeleteMessageServlet extends HttpServlet {

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//messageIdのパラメータ取得
		String messageId = request.getParameter("messageId");
		
		//MessageServiceのdeleteメソッド
		new MessageService().delete(messageId);
		
		//TOPへもどる
		response.sendRedirect("./");
	}
}
