package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import chapter6.beans.User;
import chapter6.service.UserService;

@WebServlet(urlPatterns = { "/login" })
public class LoginServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
    	
    	//login.jspにフォーワード
        request.getRequestDispatcher("login.jsp").forward(request, response);
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
    	
    	//login.jspに入力されたパラメータの取得
        String accountOrEmail = request.getParameter("accountOrEmail");
        String password = request.getParameter("password");
        
        //UserServiceのselect()メソッドでアカウントとパスが適合するか確認
        User user = new UserService().select(accountOrEmail, password);
        
        //適合しない場合はエラーメッセージをリクエストスコープに格納し、login.jspへフォワード、エラーメッセージを表示
        if (user == null) {
            List<String> errorMessages = new ArrayList<String>();
            errorMessages.add("ログインに失敗しました");
            request.setAttribute("errorMessages", errorMessages);
            request.getRequestDispatcher("login.jsp").forward(request, response);
            return;
        }
        
        //セッションに入力情報を格納（top.jspで利用）
        HttpSession session = request.getSession();
        session.setAttribute("loginUser", user);
        
        //トップ画面にリダイレクト
        response.sendRedirect("./");
    }
}