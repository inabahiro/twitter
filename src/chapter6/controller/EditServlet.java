package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.service.MessageService;


@WebServlet("/edit")
public class EditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession();
        List<String> errorMessages = new ArrayList<String>();

		//messageId取得
		String messageId = request.getParameter("messageId");
        request.setAttribute("messageId", messageId);

        //エラーメッセージの表示
        if (!isMessageIdValid(messageId, errorMessages)) {
            session.setAttribute("errorMessages", errorMessages);
            response.sendRedirect("./");
            return;
        }

        //String型を整数型に変換しidに代入
        Integer id = null;
        if(!StringUtils.isEmpty(messageId)) {
          id = Integer.parseInt(messageId);
        }

        //初期値の設定
		Message message = new MessageService().select(id);
		String messageText = message.getText();
		request.setAttribute("messageText", messageText);

    	//edit.jspにフォーワード
        request.getRequestDispatcher("edit.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession();
        List<String> errorMessages = new ArrayList<String>();

        //つぶやき更新情報、つぶやきidを取得
        String text = request.getParameter("text");
        String messageId = request.getParameter("messageId");


        //エラーメッセージの表示
        if (!isValid(text, errorMessages)) {
            session.setAttribute("errorMessages", errorMessages);
            request.setAttribute("invalidText",text );
            request.getRequestDispatcher("edit.jsp").forward(request, response);
            return;
        }

        //String型を整数型に変換しidに代入
        Integer id = null;
        if(!StringUtils.isEmpty(messageId)) {
          id = Integer.parseInt(messageId);
        }

        //messageにつぶやき情報をセット
        Message message = new Message();
        message.setText(text);
        message.setId(id);

        //MessageServiceのedit()メソッド
        new MessageService().edit(message);

        //トップへもどる
        response.sendRedirect("./");
	}

	 //バリデーション
	 private boolean isValid(String text, List<String> errorMessages) {

	    if (StringUtils.isBlank(text)) {
	        errorMessages.add("メッセージを入力してください");
	    } else if (140 < text.length()) {
	        errorMessages.add("140文字以下で入力してください");
	    }
	    if (errorMessages.size() != 0) {
	        return false;
	    }
	    return true;
	}

	 //メッセージID（URL）が有効か確かめる
	 private boolean isMessageIdValid(String messageId, List<String> errorMessages) {

		 Message message = null;

		 //IDが入力されている、かつ、IDが数字のとき型変換
		 if (!StringUtils.isEmpty(messageId) && messageId.matches("[0-9]*$")) {
			 Integer id = Integer.parseInt(messageId);
			 message = new MessageService().select(id);
		 }

		 if(message == null) {
			 errorMessages.add("不正なパラメータが入力されました");
			 return false;
		 }return true;
	 }
}


