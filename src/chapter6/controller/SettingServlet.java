package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.User;
import chapter6.exception.NoRowsUpdatedRuntimeException;
import chapter6.service.UserService;


@WebServlet(urlPatterns = { "/setting" })
public class SettingServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    	//セッションから"loginUser"を取得
        HttpSession session = request.getSession();
        User loginUser = (User) session.getAttribute("loginUser");

        //UserServiceのselectメソッド
        User user = new UserService().select(loginUser.getId());

        //userインスタンスをセッションに登録
        request.setAttribute("user", user);

        //setting.jspへフォワード
        request.getRequestDispatcher("setting.jsp").forward(request, response);
    }

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//セッションの取得
	    HttpSession session = request.getSession();

	    //エラーメッセージのList作成
	    List<String> errorMessages = new ArrayList<String>();

	    //getUserメソッドで更新情報をuserに格納
	    User user = getUser(request);

	    //更新情報がOKの場合、UserServiceのupdateメソッドの実行
	    if (isValid(user, errorMessages)) {
	        try {
	            new UserService().update(user);
	        } catch (NoRowsUpdatedRuntimeException e) {
	            errorMessages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
	        }
	    }

	    //エラーメッセージ
	    if (errorMessages.size() != 0) {
	        request.setAttribute("errorMessages", errorMessages);
	        request.setAttribute("user", user);
	        request.getRequestDispatcher("setting.jsp").forward(request, response);
	        return;
	    }

	    //ユーザー変更情報をセッションへ格納
	    session.setAttribute("loginUser", user);

	    //TOPページへリダイレクト
	    response.sendRedirect("./");
	}

	//更新情報の取得メソッド
	private User getUser(HttpServletRequest request) throws IOException, ServletException {

	    User user = new User();
	    user.setId(Integer.parseInt(request.getParameter("id")));
	    user.setName(request.getParameter("name"));
	    user.setAccount(request.getParameter("account"));
	    user.setPassword(request.getParameter("password"));
	    user.setEmail(request.getParameter("email"));
	    user.setDescription(request.getParameter("description"));
	    return user;
	}


	//バリデーション
	private boolean isValid(User user, List<String> errorMessages) {

		int id = user.getId();
	    String name = user.getName();
	    String account = user.getAccount();
	    String email = user.getEmail();

	    if (!StringUtils.isEmpty(name) && (20 < name.length())) {
	        errorMessages.add("名前は20文字以下で入力してください");
	    }
	    if (StringUtils.isEmpty(account)) {
	        errorMessages.add("アカウント名を入力してください");
	    } else if (20 < account.length()) {
	        errorMessages.add("アカウント名は20文字以下で入力してください");
	    }else {

	    	//課題③アカウントの重複
		    //ログイン中のアカウント名と同じアカウント名を再登録する場合、登録できるようにする
		    User selectedUser = new UserService().select(account);
	        if(selectedUser != null && id != selectedUser.getId()) {
	        	errorMessages.add("アカウントが重複しています");
	        }
	    }

	    if (!StringUtils.isEmpty(email) && (50 < email.length()) || StringUtils.isEmpty(email)) {
	        errorMessages.add("メールアドレスは50文字以下で入力してください");
	    }

	    if (errorMessages.size() != 0) {
	        return false;
	    }
	    return true;
	}
}