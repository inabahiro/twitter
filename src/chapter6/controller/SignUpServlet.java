package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.User;
import chapter6.service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {


    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

    	//GETの場合はsignup.jspを表示
        request.getRequestDispatcher("signup.jsp").forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)throws IOException, ServletException {

        List<String> errorMessages = new ArrayList<String>();

      //リクエストパラメータをUserオブジェクトにセット
        User user = getUser(request);

      //登録内容が不正だった場合、エラーメッセージをデータベースに保存
        if (!isValid(user, errorMessages)) {
            request.setAttribute("errorMessages", errorMessages);
            request.getRequestDispatcher("signup.jsp").forward(request, response);
            return;
        }

        //UserService.javaのinsertメソッドでDBへuserの登録
        new UserService().insert(user);

        //トップページにリダイレクト
        response.sendRedirect("./");
    }

    //User.java（Beans）のパラメータを取得するメソッド
    private User getUser(HttpServletRequest request) throws IOException, ServletException {

        User user = new User();

        user.setName(request.getParameter("name"));
        user.setAccount(request.getParameter("account"));
        user.setPassword(request.getParameter("password"));
        user.setEmail(request.getParameter("email"));
        user.setDescription(request.getParameter("description"));
        return user;
    }

    //登録内容が有効か確認するメソッド（バリデーション）
    //不正だった場合、errorMessage（リスト）にエラーメッセージを格納
    private boolean isValid(User user, List<String> errorMessages) {

    	String name = user.getName();
        String account = user.getAccount();
        String password = user.getPassword();
        String email = user.getEmail();

        if (!StringUtils.isEmpty(name) && (20 < name.length())) {
            errorMessages.add("名前は20文字以下で入力してください");
        }

        if (StringUtils.isEmpty(account)) {
            errorMessages.add("アカウント名を入力してください");
        } else if (20 < account.length()) {
            errorMessages.add("アカウント名は20文字以下で入力してください");
        }else {
        	 //課題③アカウントの重複
            User selectedUser = new UserService().select(account);
            if (selectedUser!= null) {
            	errorMessages.add("アカウントが重複しています");
            }
        }

        if (StringUtils.isEmpty(password)) {
            errorMessages.add("パスワードを入力してください");
        }

        if (!StringUtils.isEmpty(email) && (50 < email.length()) ) {
            errorMessages.add("メールアドレスは50文字以下で入力してください");
        }

        if (errorMessages.size() != 0) {
            return false;
        }
        return true;
    }
}