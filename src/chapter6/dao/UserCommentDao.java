package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import chapter6.beans.UserComment;
import chapter6.exception.SQLRuntimeException;

public class UserCommentDao {

    public List<UserComment> select(Connection connection, int num) {

        PreparedStatement ps = null;
        try {

        	//SQL文字列の結合
            StringBuilder sql = new StringBuilder();

            //表の結合
            sql.append("SELECT ");
            sql.append("comments.id as id, ");
            sql.append("comments.text as text, ");
            sql.append("comments.user_id as user_id, ");
            sql.append("comments.message_id as message_id, ");
            sql.append("users.account as account, ");
            sql.append("users.name as name, ");
            sql.append("comments.created_date as created_date ");
            sql.append("FROM comments ");
            sql.append("INNER JOIN users ");
            sql.append("ON comments.user_id = users.id ");
            sql.append("ORDER BY created_date ASC limit " + num);

            //SQL文の実行準備
            ps = connection.prepareStatement(sql.toString());

            //実行結果（テーブル）をrsに格納
            ResultSet rs = ps.executeQuery();

            //toUserCommentメソッドでつぶやきをcommentに格納
            List<UserComment> comments = toUserComment(rs);

            //commentsを返す
            return comments;

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserComment> toUserComment(ResultSet rs) throws SQLException {

        List<UserComment> comments = new ArrayList<UserComment>();

        try {
        	//テーブルを1行ずつ読み込み、commentに格納
            while (rs.next()) {

            	UserComment comment = new UserComment();
                comment.setId(rs.getInt("id"));
                comment.setText(rs.getString("text"));
                comment.setUserId(rs.getInt("user_id"));
                comment.setMessageId(rs.getInt("message_id"));
                comment.setAccount(rs.getString("account"));
                comment.setName(rs.getString("name"));
                comment.setCreatedDate(rs.getTimestamp("created_date"));

                //読み込んだデータをmessagesに入れる
                //commentsインスタンスList<UserComment>型）の中にcommentインスタンス(Comment型)が格納
                comments.add(comment);
            }
            //commentsを返す
            return comments;
        } finally {
            close(rs);
        }
    }
}
