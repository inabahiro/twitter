package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.User;
import chapter6.exception.NoRowsUpdatedRuntimeException;
import chapter6.exception.SQLRuntimeException;

public class UserDao {

	//ユーザー情報をDBに登録するメソッド（MySQLの操作）
    public void insert(Connection connection, User user) {

        PreparedStatement ps = null;
        try {

        	//SQLの文字列の結合
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO users ( ");
            sql.append("    account, ");
            sql.append("    name, ");
            sql.append("    email, ");
            sql.append("    password, ");
            sql.append("    description, ");
            sql.append("    created_date, ");
            sql.append("    updated_date ");
            sql.append(") VALUES ( ");
            sql.append("    ?, ");                                  // account
            sql.append("    ?, ");                                  // name
            sql.append("    ?, ");                                  // email
            sql.append("    ?, ");                                  // password
            sql.append("    ?, ");                                  // description
            sql.append("    CURRENT_TIMESTAMP, ");  				// created_date
            sql.append("    CURRENT_TIMESTAMP ");      				 // updated_date
            sql.append(")");

            //SQL文の実行
            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getAccount());
            ps.setString(2, user.getName());
            ps.setString(3, user.getEmail());
            ps.setString(4, user.getPassword());
            ps.setString(5, user.getDescription());

            ps.executeUpdate();

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    //入力されたログイン情報を確認
    public User select(Connection connection, String accountOrEmail, String password) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE (account = ? OR email = ?) AND password = ?";

            ps = connection.prepareStatement(sql);

            ps.setString(1, accountOrEmail);
            ps.setString(2, accountOrEmail);
            ps.setString(3, password);

            ResultSet rs = ps.executeQuery();

            List<User> users = toUsers(rs);
            if (users.isEmpty()) {
                return null;
            } else if (2 <= users.size()) {
                throw new IllegalStateException("ユーザーが重複しています");
            } else {
                return users.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<User> toUsers(ResultSet rs) throws SQLException {

        List<User> users = new ArrayList<User>();
        try {
            while (rs.next()) {
                User user = new User();
                user.setId(rs.getInt("id"));
                user.setAccount(rs.getString("account"));
                user.setName(rs.getString("name"));
                user.setEmail(rs.getString("email"));
                user.setPassword(rs.getString("password"));
                user.setDescription(rs.getString("description"));
                user.setCreatedDate(rs.getTimestamp("created_date"));
                user.setUpdatedDate(rs.getTimestamp("updated_date"));

                users.add(user);
            }
            return users;
        } finally {
            close(rs);
        }
    }

    //追記
    public User select(Connection connection, int id) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE id = ?";

            ps = connection.prepareStatement(sql);

            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();

            List<User> users = toUsers(rs);
            if (users.isEmpty()) {
                return null;
            } else if (2 <= users.size()) {
                throw new IllegalStateException("ユーザーが重複しています");
            } else {
                return users.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    //更新情報をDBに登録
    public void update(Connection connection, User user) {

        PreparedStatement ps = null;

        try {

        	//SQL文の結合
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE users SET ");
            sql.append("    account = ?, ");
            sql.append("    name = ?, ");
            sql.append("    email = ?, ");

            //課題①追加
            if(!StringUtils.isEmpty(user.getPassword())){
            	sql.append("    password = ?, ");
            }

            sql.append("    description = ?, ");
            sql.append("    updated_date = CURRENT_TIMESTAMP ");
            sql.append("WHERE id = ?");

            //SQL文（UPDATE）の準備
            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getAccount());
            ps.setString(2, user.getName());
            ps.setString(3, user.getEmail());

            //課題①追加
            if(!StringUtils.isEmpty(user.getPassword())){
            	ps.setString(4, user.getPassword());
                ps.setString(5, user.getDescription());
                ps.setInt(6, user.getId());
            }else {
                ps.setString(4, user.getDescription());
                ps.setInt(5, user.getId());
            }

            //DBに更新情報を登録
            int count = ps.executeUpdate();

            if (count == 0) {
                throw new NoRowsUpdatedRuntimeException();
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    //アカウントの重複確認
    public User select(Connection connection, String account) {

      PreparedStatement ps = null;
      try {

    	//アカウントからユーザー情報を取得
        String sql = "SELECT * FROM users WHERE account = ?";

        ps = connection.prepareStatement(sql);
        ps.setString(1, account);

        //SELECT文を実行
        ResultSet rs = ps.executeQuery();
        List<User> users = toUsers(rs);

        //ユーザーが未登録ならnullを返す
        if (users.isEmpty()) {
          return null;

        //ユーザーが2人以上なら例外を投げる
        } else if (2 <= users.size()) {
          throw new IllegalStateException("ユーザーが重複しています");

          //ユーザーが1人登録してあればユーザー情報を返す
        } else {
          return users.get(0);
        }

      } catch (SQLException e) {
        throw new SQLRuntimeException(e);
      } finally {
        close(ps);
      }
    }
}
