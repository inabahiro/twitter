package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import chapter6.beans.UserMessage;
import chapter6.exception.SQLRuntimeException;

public class UserMessageDao {

    public List<UserMessage> select(Connection connection, Integer userId, int num, String startDate,String endDate) {

        PreparedStatement ps = null;
        try {

        	//SQL文字列の結合
            StringBuilder sql = new StringBuilder();

            //表の結合
            sql.append("SELECT ");
            sql.append("messages.id as id, ");
            sql.append("messages.text as text, ");
            sql.append("messages.user_id as user_id, ");
            sql.append("users.account as account, ");
            sql.append("users.name as name, ");
            sql.append("messages.created_date as created_date ");
            sql.append("FROM messages ");
            sql.append("INNER JOIN users ");
            sql.append("ON messages.user_id = users.id ");

            if(userId != null) {
            	sql.append("WHERE users.id = ? ");
            }else {
            	sql.append("WHERE messages.created_date BETWEEN ? AND ? ");
            }

            sql.append("ORDER BY created_date DESC limit " + num);

            //SQL文の実行準備
            ps = connection.prepareStatement(sql.toString());

            //バインド変数
            if(userId != null) {
                ps.setInt(1, userId);
            }else {
            	ps.setString(1, startDate);
            	ps.setString(2, endDate);
            }

            //実行結果（テーブル）をrsに格納
            ResultSet rs = ps.executeQuery();

            //toUserMessageメソッドでつぶやきをmessageに格納
            List<UserMessage> messages = toUserMessages(rs);

            //messageを返す
            return messages;

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserMessage> toUserMessages(ResultSet rs) throws SQLException {

    	//リストを用意
        List<UserMessage> messages = new ArrayList<UserMessage>();

        try {
        	//テーブルを1行ずつ読み込み、messageに格納
            while (rs.next()) {
            	UserMessage message = new UserMessage();
                message.setId(rs.getInt("id"));
                message.setText(rs.getString("text"));
                message.setUserId(rs.getInt("user_id"));
                message.setAccount(rs.getString("account"));
                message.setName(rs.getString("name"));
                message.setCreatedDate(rs.getTimestamp("created_date"));

                //読み込んだデータをmessagesに入れる
                //messagesインスタンス（List<UserMessage>型）の中にmessageインスタンス(Message型)が格納
                messages.add(message);
            }

            //messagesを返す
            return messages;
        } finally {
            close(rs);
        }
    }
}