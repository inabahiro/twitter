<%@ page language = "java" contentType = "text/html; charset = UTF-8" pageEncoding = "UTF-8" %>

<%--EL式を利用するかの定義--%>
<%@ page isELIgnored = "false" %>

<%--タグライブラリの指定--%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fmt" uri= "http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>簡易Twitter</title>
        <link href="./css/style.css" rel="stylesheet" type="text/css">
   		<script src="./js/vender/jquery-3.6.0.min.js"></script>
   		<script src="./js/main.js"></script>
    </head>
    <body>
        <div class="main-contents">

       		 <!--ログイン、登録ボタン--->
        	<div class="header">
        		<c:if test="${ empty loginUser }">
                	<a href="login">ログイン</a>
                	<a href="signup">登録する</a>
                </c:if>
   				<c:if test="${ not empty loginUser }">
    			    <a href="./">ホーム</a>
        			<a href="setting">設定</a>
      			 	<a href="logout">ログアウト</a>
    			</c:if>
            </div>

            <!--絞り込みフォーム-->
            <div class="select_form">
            	<c:if test="${ not empty loginUser }">
        			<form action="index.jsp" method="get" >
        				<input type="date" name="start" value="${start}">
        				～
            			<input type="date" name="end" value="${end}">
            			<input type="submit" value="絞り込み">
        			</form>
        		</c:if>
			</div>

            <!--ログインユーザーの情報を表示-->
            <div class="profile">
	            <c:if test="${ not empty loginUser }">
   			    	<div class="name"><h2><c:out value="${loginUser.name}" /></h2></div>
        			<div class="account">@<c:out value="${loginUser.account}" /></div>
        			<div class="description"><c:out value="${loginUser.description}" /></div>
				</c:if>
			</div>

			<!--エラーメッセージの表示-->
			<c:if test="${ not empty errorMessages }">
    			<div class="errorMessages">
        			<ul>
            			<c:forEach items="${errorMessages}" var="errorMessage">
                			<li><c:out value="${errorMessage}" />
            			</c:forEach>
        			</ul>
    			</div>
    			<c:remove var="errorMessages" scope="session" />
			</c:if>

			<!--つぶやきフォーム-->
			<div class="form-area">
				<!-- ログイン中のみフォームを表示 -->
    			<c:if test="${ isShowMessageForm }">
        			<form action="message" method="post">
            			いま、どうしてる？<br />
            			<textarea name="text" cols="100" rows="5" class="tweet-box">${invalidMessage}</textarea>
            			<c:remove var="invalidMessage" scope="session" />
            			<br />
            			<input type="submit" value="つぶやく">（140文字まで）
        			</form>
    			</c:if>
			</div>


			<!--つぶやき、返信-->
			<div class="messages">
    			<c:forEach items="${messages}" var="message">

        			<!--つぶやきの表示-->
        			<div class="message">
            			<div class="account-name">
                			<span class="account">

                				<!--課題②-->
                				<a href="./?user_id=<c:out value="${message.userId}"/> ">
                					<c:out value="${message.account}" />
                				</a>
                			</span>
                			<span class="name">
                				<c:out value="${message.name}" />
                			</span>
            			</div>

	            		<div class="text">
            				<pre><c:out value="${message.text}" /></pre>
            			</div>

            			<div class="date">
            				<fmt:formatDate value="${message.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" />
            			</div>


						<div style="display:inline-flex">
						<!-- 編集ボタン -->
						<div class = "edit">
	            			<c:if test = "${message.userId == loginUser.id}">
      							<form action= "edit" method = "get">
            					<input type="submit" value= "編集"/>
            					<input type="hidden" name="messageId" value="${message.id}"/>
            					</form>
							</c:if>
						</div>

						<!-- 削除ボタン -->
						<div class = "delete">
	            			<c:if test = "${message.userId == loginUser.id}">
      							<form action= "deleteMessage" method = "post">
            						<input type="hidden" name="messageId" value="${message.id}"/>
            						<input type="submit" value= "削除"/>
            				</form>
							</c:if>
						</div>
						</div>
        			</div>

					<!--エラーメッセージの表示-->
					<c:if test="${ not empty errorMessages }">
    					<div class="errorMessages">
        					<ul>
            					<c:forEach items="${errorMessages}" var="errorMessage">
                					<li><c:out value="${errorMessage}" />
            					</c:forEach>
        					</ul>
    					</div>
    					<c:remove var="errorMessages" scope="session" />
					</c:if>

					<!--コメントの表示-->
					<div class="comments">
						<c:forEach items="${comments}" var="comment">
							<c:if test = "${message.id == comment.messageId}">
            					<div class="account-name">
                					<span class="account">
                						<c:out value="${comment.account}" />
                					</span>
                						<span class="name">
                						<c:out value="${comment.name}" />
                					</span>
            					</div>
	            				<div class="text">
            						<pre><c:out value="${comment.text}" /></pre>
            					</div>
            					<div class="date">
            						<fmt:formatDate value="${comment.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" />
            					</div>
            					</c:if>
            				</c:forEach>
            		</div>
       				<!-- 返信フォーム -->
        			<div class="comment_form">
        				<!-- ログイン中のみフォームを表示 -->
        				<c:if test="${ isShowMessageForm }">
        					<form action="comment" method="post">
        						返信
        						<br />
            					<textarea name="comment" cols="100" rows="5" class="tweet-box"></textarea>
            					<br />
            					<input type="hidden" name="messageId" value="${message.id}"/>
            					<input type="submit" value="返信">（140文字まで）
        					</form>
        				</c:if>
					</div>

				</c:forEach>
			</div>

            <div class="copyright"> Copyright(c)Hiro Inaba</div>
        </div>
    </body>
</html>

