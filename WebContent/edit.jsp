<%@ page language = "java" contentType = "text/html; charset = UTF-8" pageEncoding = "UTF-8" %>

<%--EL式を利用するかの定義--%>
<%@ page isELIgnored = "false" %>

<%--タグライブラリの指定--%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fmt" uri= "http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>編集</title>
        <link href="./css/style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="main-contents">

			<!--エラーメッセージの表示-->
			<c:if test="${ not empty errorMessages }">
    			<div class="errorMessages">
        			<ul>
            			<c:forEach items="${errorMessages}" var="errorMessage">
                			<li><c:out value="${errorMessage}" />
            			</c:forEach>
        			</ul>
    			</div>
			</c:if>

			<!--つぶやき更新フォーム-->
			<div class="form-area">
        			<form action="edit" method= "post">
            			つぶやき<br />

            				<c:choose>
            					<c:when test="${ not empty errorMessages }">
            						<textarea name="text" cols="100" rows="5" class="tweet-box">${invalidText}</textarea><br />
            					</c:when>
            					<c:otherwise>
            						<textarea name="text" cols="100" rows="5" class="tweet-box">${messageText}</textarea><br />
            					</c:otherwise>
            				</c:choose>
            				<c:remove var="errorMessages" scope="session" />
            			<input type="submit" value="更新">（140文字まで）<br />
            			<input type="hidden" name="messageId" value="${messageId}"/>
            			<a href="./">戻る</a>
        			</form>
    		</div>
            <div class="copyright"> Copyright(c)Hiro Inaba</div>

		</div>
    </body>
</html>

